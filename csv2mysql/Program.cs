﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace csv2mysql
{
    class Program
    {
        static void Main(string[] args)
        {
            //check input file
            // string filepath = args.Length > 0 ? args[0] : ""/*"Machine2.txt"*/;
            string[] machine_array = { "EM2510NT", "EM2510NT-NEW",
            "EM2510NT-ASR", "FOMIIRI3015NT", "EML3510NT-TK", 
            "ACIES2512-MTK", "ACIES2512-MTK2", "FLC2412AJ",
            "LC2012C1NT", "HDS8025NT-1", "HDS8025NT-2", "HDS8025NT-3",
            "HDS8025NT-4", "HDS8025NT-5", "HDS8025NT-6", "HDS8025NT-7",
            "HDS8025NT-8","HDS8025NT-9","HDS8025NT-10","HDS8025NT-11","HDS1303NT-1",
            "HDS1303NT-2","HD1703LNT","FBDITI1025NT-1","HG2204","HG-ATC-1","HG-ATC-2",
            "HG-ATC-3","HG-ATC-4","HG-Ars","ASR48M-TK","EM2510NT-4","EM23510NT"};
            // string filepath = @"Machine2.txt"; 
            foreach (var mname in machine_array)
            {
                string filepath = mname + ".txt";
                if (!File.Exists(filepath))
                {
                    Console.WriteLine("The input file does not exist.");
                    return;
                }

                string machinename = Path.GetFileNameWithoutExtension(filepath);

                //check input file starts with 0
                string text = System.IO.File.ReadAllText(filepath);
                if (text.StartsWith("0"))
                {
                    System.IO.File.WriteAllText("_temp.txt", text.Substring(1));

                    filepath = "_temp.txt";
                }
                else
                {
                    Console.WriteLine("The input file is not one for dumping to mysql.");
                    return;
                }

                MySqlConnection con = new MySqlConnection("Server = localhost; user = root; password = ");
                MySqlDataReader rdr = null;
                MySqlDataReader rdr1 = null;

                try
                {
                    con.Open();
                    string sql = "CREATE DATABASE IF NOT EXISTS `factory`;";
                    MySqlCommand cmd0 = new MySqlCommand(sql, con);
                    cmd0.ExecuteNonQuery();

                    sql = " CREATE TABLE IF NOT EXISTS `factory`.`working_analysis` (" +
                                  "`MachinNAME` varchar(255) DEFAULT NULL," +
                                  "`Date` date DEFAULT NULL," +
                                  "`TotalWorkingHrs` float DEFAULT NULL," +
                                  "`TotalAlarmHrs` float DEFAULT NULL," +
                                  "`TotalPlanningHrs` float DEFAULT NULL," +
                                  "`TotalStandbyHrs` float DEFAULT NULL," +
                                  "`TotalPowerOffHrs` float DEFAULT NULL" +
                                  ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
                    MySqlCommand cmd1 = con.CreateCommand();
                    cmd1.CommandText = sql;
                    cmd1.ExecuteNonQuery();

                    sql = " CREATE TABLE IF NOT EXISTS `factory`.`working_analysis_history` (" +
                                  "`MachinNAME` varchar(255) DEFAULT NULL," +
                                  "`Date` date DEFAULT NULL," +
                                  "`TotalWorkingHrs` float DEFAULT NULL," +
                                  "`TotalAlarmHrs` float DEFAULT NULL," +
                                  "`TotalPlanningHrs` float DEFAULT NULL," +
                                  "`TotalStandbyHrs` float DEFAULT NULL," +
                                  "`TotalPowerOffHrs` float DEFAULT NULL" +
                                  ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
                    MySqlCommand cmd1_1 = con.CreateCommand();
                    cmd1_1.CommandText = sql;
                    cmd1_1.ExecuteNonQuery();

                    sql = " CREATE TABLE IF NOT EXISTS `factory`.`last_updated_info` (" +
                                  "`MachinNAME` varchar(255) DEFAULT NULL," +
                                  "`Date` date DEFAULT NULL " +
                                  ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
                    MySqlCommand cmd1_2 = con.CreateCommand();
                    cmd1_2.CommandText = sql;
                    cmd1_2.ExecuteNonQuery();

                    sql = " CREATE TABLE IF NOT EXISTS `factory`.`working_history` (" +
                            "`MachinNAME` varchar(255) DEFAULT NULL," +
                            " `_status` varchar(255) DEFAULT NULL," +
                             " `_date` date DEFAULT NULL," +
                             " `_time` time DEFAULT NULL," +
                             " `_desc_id` varchar(10) DEFAULT NULL," +
                             " `_desc_msg` varchar(255) DEFAULT NULL" +
                            " ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                    MySqlCommand cmd2 = con.CreateCommand();
                    cmd2.CommandText = sql;
                    cmd2.ExecuteNonQuery();

                    sql = " CREATE TABLE IF NOT EXISTS `factory`.`tmp_working_history` (" +
                            " `_status` varchar(255) DEFAULT NULL," +
                             " `_date` date DEFAULT NULL," +
                             " `_time` time DEFAULT NULL," +
                             " `_desc_id` varchar(10) DEFAULT NULL," +
                             " `_desc_msg` varchar(255) DEFAULT NULL" +
                            " ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                    MySqlCommand cmd2_1 = con.CreateCommand();
                    cmd2_1.CommandText = sql;
                    cmd2_1.ExecuteNonQuery();

                    sql = " DELETE FROM `factory`.`tmp_working_history`";

                    MySqlCommand cmd2_2 = con.CreateCommand();
                    cmd2_2.CommandText = sql;
                    cmd2_2.ExecuteNonQuery();

                    Console.WriteLine("Loadng data from text file to mysql database...");

                    sql = " LOAD DATA LOCAL INFILE  '" + filepath + "' INTO TABLE `factory`.tmp_working_history FIELDS TERMINATED BY ','  ENCLOSED BY '\"' LINES TERMINATED BY '\n' ";
                    MySqlCommand cmd3 = con.CreateCommand();
                    cmd3.CommandText = sql;
                    cmd3.ExecuteNonQuery();

                    sql = "DELETE FROM `factory`.working_history WHERE MachinNAME='" + machinename + "'";
                    MySqlCommand cmd3_2 = con.CreateCommand();
                    cmd3_2.CommandText = sql;
                    cmd3_2.ExecuteNonQuery();

                    sql = "INSERT INTO `factory`.working_history(MachinNAME, _status, _date, _time, _desc_id, _desc_msg) " +
                          "SELECT '" + machinename + "',  _status, _date, _time, _desc_id, _desc_msg FROM `factory`.tmp_working_history";

                    MySqlCommand cmd3_1 = con.CreateCommand();
                    cmd3_1.CommandText = sql;
                    cmd3_1.ExecuteNonQuery();

                    Console.WriteLine("Calculating...");


                    sql = "SELECT DISTINCT _status, _date, _time From `factory`.tmp_working_history order by _date, _time";
                    MySqlCommand cmd5 = new MySqlCommand(sql, con);
                    rdr = cmd5.ExecuteReader();

                    string today = "";
                    string datetime = "", old_datetime = "";
                    string status = "", old_status = "";

                    long diffSec = 0;
                    double TotalWorkingHrs = 0;
                    double TotalAlarmHrs = 0;
                    double TotalPlanningHrs = 0;
                    double TotalStandbyHrs = 0;
                    double TotalPowerOffHrs = 0;

                    while (rdr.Read())
                    {
                        if (old_status == "")
                        {
                            old_status = rdr.GetString(0);
                            old_datetime = DateTime.Parse(rdr.GetString(1)).Date.ToString("yyyy-MM-dd") + " " + rdr.GetString(2);
                            today = DateTime.Parse(rdr.GetString(1)).Date.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            status = rdr.GetString(0);
                            datetime = DateTime.Parse(rdr.GetString(1)).Date.ToString("yyyy-MM-dd") + " " + rdr.GetString(2);

                            TimeSpan diffTs = DateTime.Parse(datetime) - DateTime.Parse(old_datetime);
                            diffSec = diffTs.Days * 24 * 3600 + diffTs.Hours * 3600 + diffTs.Minutes * 60 + diffTs.Seconds;

                            if (old_status == "Working")
                            {
                                TotalWorkingHrs += (diffSec / 3600.0);
                                System.Diagnostics.Debug.WriteLine("Working => " + datetime + " - " + old_datetime);
                            }
                            else if (old_status == "Standby")
                            {
                                TotalStandbyHrs += (diffSec / 3600.0);
                            }
                            else if (old_status == "Alarm")
                            {
                                TotalAlarmHrs += (diffSec / 3600.0);
                            }
                            else if (old_status == "PowerOFF")
                            {
                                TotalPowerOffHrs += (diffSec / 3600.0);
                            }
                            else if (old_status == "Planning")
                            {
                                TotalPlanningHrs += (diffSec / 3600.0);
                            }

                            old_status = status;
                            old_datetime = datetime;
                        }
                    }

                    rdr.Close();

                    Console.WriteLine("Inserting the calculated data to mysql database...");

                    sql = "SELECT * From `factory`.last_updated_info WHERE MachinNAME = '" + machinename + "' AND Date = '" + today + "'";
                    MySqlCommand cmd6 = new MySqlCommand(sql, con);
                    rdr1 = cmd6.ExecuteReader();

                    if (!rdr1.Read())
                    {
                        rdr1.Close();

                        sql = "INSERT INTO `factory`.working_analysis_history " +
                              "SELECT * FROM `factory`.working_analysis WHERE MachinNAME = '" + machinename + "'";

                        MySqlCommand cmd8 = con.CreateCommand();
                        cmd8.CommandText = sql;
                        cmd8.ExecuteNonQuery();

                        MySqlCommand cmd7 = new MySqlCommand();
                        cmd7.Connection = con;
                        cmd7.CommandText = "INSERT INTO `factory`.last_updated_info(MachinNAME, Date) VALUES(@MachinNAME, @Date)";
                        cmd7.Prepare();
                        cmd7.Parameters.AddWithValue("@MachinNAME", machinename);
                        cmd7.Parameters.AddWithValue("@Date", today);
                        cmd7.ExecuteNonQuery();
                    }
                    else
                    {
                        rdr1.Close();
                    }

                    sql = "DELETE FROM `factory`.working_analysis WHERE MachinNAME='" + machinename + "'";
                    MySqlCommand cmd4_1 = con.CreateCommand();
                    cmd4_1.CommandText = sql;
                    cmd4_1.ExecuteNonQuery();

                    MySqlCommand cmd4 = new MySqlCommand();
                    cmd4.Connection = con;
                    cmd4.CommandText = "INSERT INTO `factory`.working_analysis(MachinNAME, Date, TotalWorkingHrs, TotalAlarmHrs, TotalPlanningHrs, TotalStandbyHrs, TotalPowerOffHrs) VALUES(@MachinNAME, @Date, @TotalWorkingHrs, @TotalAlarmHrs, @TotalPlanningHrs, @TotalStandbyHrs, @TotalPowerOffHrs)";
                    cmd4.Prepare();

                    cmd4.Parameters.AddWithValue("@MachinNAME", machinename);
                    cmd4.Parameters.AddWithValue("@Date", today);
                    cmd4.Parameters.AddWithValue("@TotalWorkingHrs", TotalWorkingHrs);
                    cmd4.Parameters.AddWithValue("@TotalAlarmHrs", TotalAlarmHrs);
                    cmd4.Parameters.AddWithValue("@TotalPlanningHrs", TotalPlanningHrs);
                    cmd4.Parameters.AddWithValue("@TotalStandbyHrs", TotalStandbyHrs);
                    cmd4.Parameters.AddWithValue("@TotalPowerOffHrs", TotalPowerOffHrs);

                    cmd4.ExecuteNonQuery();

                    Console.WriteLine("Requet completed! Please check 'working_history', 'working_analysis' tables in 'factory' database ");

                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error : {0}", ex.ToString());
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }

                    File.Delete("_temp.txt");
                }
            }
        }
    }
}
